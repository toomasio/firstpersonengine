﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class Controller : MonoBehaviour
{
    public enum UnitState { Idle, InAir, Walking, Running, Crouching, InAirCrouching, WallRunning }
    private UnitState curUnitState;
    public UnitState CurUnitState { get { return curUnitState; } }
    private UnitState lastState;

    [SerializeField] private string inputAxisLeftRight = "Horizontal";
    [SerializeField] private string inputAxisForBack = "Vertical";
    [SerializeField] private float speed = 6;
    private float curSpeed;
    //running
    [SerializeField] private bool enableRun;
    [SerializeField] private string inputButtonRun = "Run";
    [SerializeField] private bool onlyRunForward;
    [SerializeField] private float runSpeed = 10;
    private BoolWrapper run = new BoolWrapper(false);
    private bool running;
    //wall running
    [SerializeField] private bool enableWallRunning;
    [SerializeField] private float zRotAmount = 45;
    [SerializeField] private float transitionTime = 0.2f;
    [SerializeField] private float upwardPushAssist = 0.1f;

    private bool wallRunning;
    //crouching
    [SerializeField] private bool enableCrouch;
    [SerializeField] private string inputButtonCrouch = "Crouch";
    [SerializeField] private float crouchSpeed = 3;
    [SerializeField] private float crouchSizeTime = 0.1f;
    [SerializeField] private float crouchSpeedTime = 1;
    [SerializeField] private float crouchSize = 0.5f;
    private BoolWrapper crouch = new BoolWrapper(false);
    private bool crouching;
    //rotation
    [SerializeField] private string inputAxisLookLeftRight = "Mouse X";
    [SerializeField] private string inputAxisLookUpDown = "Mouse Y";
    [SerializeField] private float rotateSensitivity = 10;
    [SerializeField] private Transform controllerCamera;
    [SerializeField] private bool invertLook;
    [SerializeField] private float camRotateLimit = 85;
    private float curCamRotX;
    private float curCamRotY;
    //jumping
    [SerializeField] private string inputButtonJump = "Jump";
    [SerializeField] private float jumpPower = 10;
    private bool jump;
    private bool jumpingUp;
    [SerializeField] private float gravityMultiplier = 2;
    [SerializeField] private float inAirControlFadeInTime = 1;
    private bool controlFading;
    //ground detection
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private Vector3 groundBoxSize = Vector3.one;
    [SerializeField] private Vector3 groundBoxCenter = Vector3.zero;
    public Vector3 curGroundCenter;
    private Collider[] groundCols;
    private BoolWrapper grounded = new BoolWrapper(false);
    private BoolWrapper inAir = new BoolWrapper(false);
    public bool IsGrounded { get { return grounded.Value; } }
    //ceiling detection
    [SerializeField] private LayerMask ceilingMask;
    [SerializeField] private Vector3 ceilingBoxSize = Vector3.one;
    [SerializeField] private Vector3 ceilingBoxCenter = Vector3.zero;
    public Vector3 curCeilingCenter;
    private Collider[] ceilingCols;
    private BoolWrapper hitCeiling = new BoolWrapper(false);
    public bool IsHitCeiling { get { return hitCeiling.Value; } }
    //wall detection
    [SerializeField] private LayerMask wallMask;
    [SerializeField] private bool stopMovementOnWallHit;
    [SerializeField] private float stopPercent = 0.5f;
    public float curWallCheckRadius = 0.5f;
    public Vector3 curWallCheckPoint1;
    public Vector3 curWallCheckPoint2;
    private Collider[] wallHits;
    private bool wallHit;
    //wall Jump detection
    [SerializeField] private Vector3 wallRunGroundBoxSize = Vector3.one;
    [SerializeField] private Vector3 wallRunGroundBoxCenter = Vector3.zero;

    //comps
    private Rigidbody rb;
    private MenuManager mm;
    private CapsuleCollider col;

    //movement vars
    private float inputHor;
    private float inputVer;
    private Vector3 move;
    private Vector3 inputWorldDirection;
    private bool inWallRunRange;
    private Vector2 localInputDirection;

    //rotate vars
    private float mouseX;
    private float mouseY;
    private Vector3 hRotate;
    private Vector3 vRotate;

    //collider vars
    private float startHeight;
    private Vector3 startCenter;
    private Vector3 startCamPos;

    //inspector gui
    [HideInInspector][SerializeField] private int inputForBack;
    [HideInInspector][SerializeField] private int inputLeftRight;
    [HideInInspector][SerializeField] private int inputLookLeftRight;
    [HideInInspector][SerializeField] private int inputLookUpDown;
    [HideInInspector][SerializeField] private int inputRun;
    [HideInInspector][SerializeField] private int inputCrouch;
    [HideInInspector][SerializeField] private int inputJump;

    // Use this for initialization
    void Start ()
    {
        GetComponents ();
        SetupController ();
    }

    void GetComponents ()
    {
        rb = GetComponent<Rigidbody> ();
        mm = GameManager.instance.GetMenuManager ();
        col = GetComponent<CapsuleCollider> ();
    }

    void SetupController ()
    {
        curSpeed = speed;
        startHeight = col.height;
        startCenter = col.center;
        startCamPos = controllerCamera.localPosition;
        curGroundCenter = groundBoxCenter;
        curCeilingCenter = ceilingBoxCenter;
    }

    private void Update ()
    {
        if (mm.IsPaused ())
        {
            return;
        }

        GetInputs ();
        CheckMovementState ();
        SetMovementState ();
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        if (mm.IsPaused ())
        {
            return;
        }

        CheckGrounded ();
        CheckCeiling ();
        CheckWallHits ();
        MoveController ();
        RotateController ();
        RotateCamera ();
        AddGravity ();
    }

    void GetInputs ()
    {

        //movement inputs
        inputHor = Input.GetAxisRaw (inputAxisLeftRight);
        inputVer = Input.GetAxisRaw (inputAxisForBack);
        localInputDirection = new Vector2 (inputHor, inputVer).normalized;
        Vector3 hMove = transform.right * inputHor;
        Vector3 zMove = transform.forward * inputVer;
        inputWorldDirection = (hMove + zMove).normalized;

        //mouse inputs
        mouseX = Input.GetAxisRaw (inputAxisLookLeftRight);
        mouseY = Input.GetAxisRaw (inputAxisLookUpDown);
        hRotate = transform.up * mouseX;
        vRotate = Vector3.right * mouseY;

        //wall run range
        float localInputX = Mathf.Abs (localInputDirection.x);
        inWallRunRange = localInputX > 0.60f && localInputX < 0.80f;

        //running
        run.Value = Input.GetButton (inputButtonRun);

        //crouching
        if (Input.GetButtonDown (inputButtonCrouch))
            crouch.Value = !crouch.Value;

        //jumping
        jump = Input.GetButtonDown (inputButtonJump);
        if (jump)
            Jump ();
    }

    void CheckGrounded ()
    {
        groundCols = Physics.OverlapBox (transform.TransformPoint(curGroundCenter), groundBoxSize / 2, transform.rotation, groundMask);
        grounded.Value = (groundCols.Length > 0);
        inAir.Value = !grounded.Value;
    }

    void CheckCeiling ()
    {
        ceilingCols = Physics.OverlapBox (transform.TransformPoint(curCeilingCenter), ceilingBoxSize / 2, transform.rotation, ceilingMask);
        if (ceilingCols.Length > 0)
        {
            hitCeiling.Value = true;
        }
        else if (hitCeiling.Value)
            hitCeiling.Value = false;
    }

    void CheckWallHits ()
    {
        if (!stopMovementOnWallHit)
            return;

        curWallCheckRadius = col.radius * 0.7f;
        Vector3 extentsY = new Vector3(0, col.bounds.extents.y, 0);
        curWallCheckPoint1 = (inputWorldDirection * 0.3f) + transform.TransformPoint((extentsY - (col.center * 0.25f)));
        curWallCheckPoint2 = (inputWorldDirection * 0.3f) + transform.TransformPoint((extentsY + (col.center * 0.25f)));

        wallHits = Physics.OverlapCapsule (curWallCheckPoint1, curWallCheckPoint2, curWallCheckRadius, wallMask);
        if (wallHits.Length > 0)
            wallHit = true;
        else if (wallHit)
            wallHit = false;
    }

    void SetMovementState ()
    {
        if (run.Value && wallHit && inWallRunRange)
        {
            curUnitState = UnitState.WallRunning;
        }
        else if (wallRunning)
            curUnitState = UnitState.WallRunning;
        else if (grounded.Value)
        {
            if (crouch.Value)
                curUnitState = UnitState.Crouching;
            else if (inputWorldDirection != Vector3.zero)
            {
                if (run.Value)
                    curUnitState = UnitState.Running;
                else
                    curUnitState = UnitState.Walking;

            }
            else
                curUnitState = UnitState.Idle;

        }
        else if (crouch.Value)
            curUnitState = UnitState.InAirCrouching;
        else
            curUnitState = UnitState.InAir;
    }

    void CheckMovementState ()
    {
        switch (curUnitState)
        {
            case UnitState.Idle:
                Idle ();
                break;
            case UnitState.InAir:
                InAir ();
                break;
            case UnitState.Walking:
                Walk ();
                break;
            case UnitState.Running:
                Run ();
                break;
            case UnitState.Crouching:
                GroundCrouch ();
                break;
            case UnitState.InAirCrouching:
                InAirCrouch ();
                break;
            case UnitState.WallRunning:
                WallRun ();
                break;
        }
        lastState = curUnitState;
    }

    void Idle ()
    {
        curSpeed = speed;
    }

    void InAir ()
    {      
        if (!controlFading)
            StartCoroutine (StartControlFade (inAirControlFadeInTime, grounded));
    }

    void Walk ()
    {
        curSpeed = speed;
    }

    void Run ()
    {
        if (!enableRun)
            return;

        if (onlyRunForward && inputVer <= 0)
            return;

        curSpeed = runSpeed;
    }

    void WallRun ()
    {

        if (!inWallRunRange || !run.Value || jump || !CheckWallRunningGrounded())
        {
            StopWallRunning ();
            return;
        }

        if (wallRunning)
            return;

        float zRot = inputHor * zRotAmount;
        StartCoroutine (StartWallRunTransition (zRot, transitionTime));
        wallRunning = true;

    }

    private bool CheckWallRunningGrounded()
    {
        Collider[] cols = Physics.OverlapBox(transform.TransformPoint(wallRunGroundBoxCenter), wallRunGroundBoxSize / 2, transform.rotation, wallMask);
        if (cols.Length > 0)
            return true;
        else
            return false;
    }

    void StopWallRunning ()
    {
        StopCoroutine("StartWallRunTransition");

        StartCoroutine(StartWallRunTransition(0,transitionTime));
        StartCoroutine(StartResetCamera(transitionTime));
        StartCoroutine(StartSpeedFade(0, curSpeed, inAirControlFadeInTime, inAir));
        wallRunning = false;
    }

    void GroundCrouch ()
    {
        if (!enableCrouch)
            return;

        if (!crouch.Value)
        {
            ResetCrouch ();
            return;
        }

        if (crouching)
            return;

        Crouch (transform.position);
        //slide if sprinting
        if (lastState == UnitState.Running)
            StartCoroutine (StartSpeedFade (curSpeed, crouchSpeed, crouchSpeedTime, new BoolWrapper (true)));
        else
            curSpeed = crouchSpeed;
    }

    void InAirCrouch ()
    {
        if (!enableCrouch)
            return;

        if (!crouch.Value)
        {
            ResetCrouch ();
            return;
        }

        Crouch (transform.position + (startCamPos * crouchSize));
    }

    void Crouch (Vector3 _transPos)
    {
        if (crouching)
            return;

        StopCoroutine ("StartCrouch");
        StartCoroutine (StartCrouch (_transPos, ceilingBoxCenter * crouchSize, startCamPos * crouchSize, startHeight * crouchSize, startCenter * crouchSize));
        crouching = true;
    }

    void ResetCrouch ()
    {
        if (hitCeiling.Value)
        {
            crouch.Value = true;
            return;
        }

        StopCoroutine ("StartCrouch");
        StartCoroutine (StartCrouch (transform.position, ceilingBoxCenter, startCamPos, startHeight, startCenter));
        crouching = false;
    }

    void AddGravity ()
    {
        if (wallRunning)
        rb.velocity += transform.up * Physics.gravity.y * gravityMultiplier * Time.deltaTime;
        else
        rb.velocity += Vector3.up * Physics.gravity.y * gravityMultiplier * Time.deltaTime;
    }

    void MoveController ()
    {
        if (!controlFading && !wallRunning)
            move = inputWorldDirection;

        Vector3 rbMove = rb.position + (move * Time.deltaTime * curSpeed);

        if (!jumpingUp && wallHit && !wallRunning)
        {
            rbMove = Vector3.Lerp (rb.position + (move * Time.deltaTime * curSpeed), rb.position, stopPercent);
        }

        rb.MovePosition (rbMove);
    }
    void RotateController ()
    {
        if (wallRunning)
            return;

        rb.MoveRotation (Quaternion.Euler (transform.eulerAngles + (hRotate * rotateSensitivity)));
    }

    void RotateCamera ()
    {
        if (invertLook)
            vRotate = -vRotate;

        float camRotX = vRotate.x * rotateSensitivity;
        curCamRotX -= camRotX;
        curCamRotX = Mathf.Clamp (curCamRotX, -camRotateLimit, camRotateLimit);
 
        if (wallRunning)
        {
            float camRotY = hRotate.y * rotateSensitivity;
            curCamRotY += camRotY;
            curCamRotY = Mathf.Clamp (curCamRotY, -camRotateLimit, camRotateLimit);
        }

        Vector2 camRot = new Vector2 (curCamRotX, curCamRotY);

        controllerCamera.transform.localEulerAngles = camRot;

    }
    void Jump ()
    {
        if (crouching && hitCeiling.Value)
            return;

        if (grounded.Value || CheckWallRunningGrounded())
        {
            rb.velocity = transform.up * jumpPower;
            StartCoroutine (StartJumpingUp ());
        }
    }

    void RunTimer (float _time, FloatWrapper _timer, FloatWrapper _perc)
    {
        _timer.Value += Time.deltaTime;
        if (_timer.Value > _time)
            _timer.Value = _time;
        _perc.Value = _timer.Value / _time;
    }

    IEnumerator StartWallRunTransition (float _endZRot, float _time)
    {
        FloatWrapper timer = new FloatWrapper (0);
        FloatWrapper perc = new FloatWrapper (0);
        Quaternion startRot = Quaternion.Euler(transform.localEulerAngles);
        Quaternion camStartRot = controllerCamera.rotation;
        Vector3 startMove = move;
        while (timer.Value < _time)
        {
            RunTimer (_time, timer, perc);
            Quaternion endRot = Quaternion.Euler(0, camStartRot.eulerAngles.y, _endZRot);
            transform.localRotation = Quaternion.Slerp(startRot, endRot, perc.Value);
            move = Vector3.Lerp(startMove, (transform.forward + (transform.right * (inputHor * upwardPushAssist))), perc.Value);
            yield return new WaitForFixedUpdate ();
        }
    }

    IEnumerator StartResetCamera(float _time)
    {
        FloatWrapper timer = new FloatWrapper (0);
        FloatWrapper perc = new FloatWrapper (0);
        float startCamY = curCamRotY;

        while (timer.Value < _time)
        {
            RunTimer (_time, timer, perc);
            curCamRotY = Mathf.LerpAngle(startCamY, 0, perc.Value);
            yield return new WaitForFixedUpdate ();
        }
    }

    IEnumerator StartJumpingUp ()
    {
        jumpingUp = true;
        while (rb.velocity.y >= 0)
        {
            yield return new WaitForEndOfFrame ();
        }
        jumpingUp = false;
    }

    IEnumerator StartCrouch (Vector3 _transPos, Vector3 _endCeilingCenter, Vector3 _camEndPos, float _endHeight, Vector3 _endColCenter)
    {
        FloatWrapper timer = new FloatWrapper (0);
        FloatWrapper perc = new FloatWrapper (0);
        Vector3 camPos = controllerCamera.localPosition;
        float height = col.height;
        Vector3 colCenter = col.center;
        Vector3 pos = transform.position;
        Vector3 ceilingCenter = curCeilingCenter;
        while (timer.Value < crouchSizeTime)
        {
            RunTimer (crouchSizeTime, timer, perc);
            transform.position = Vector3.Lerp (pos, new Vector3 (transform.position.x, _transPos.y, transform.position.z), perc.Value);
            controllerCamera.localPosition = Vector3.Lerp (camPos, _camEndPos, perc.Value);
            col.height = Mathf.Lerp (col.height, _endHeight, perc.Value);
            col.center = Vector3.Lerp (colCenter, _endColCenter, perc.Value);
            curCeilingCenter = Vector3.Lerp (ceilingCenter, _endCeilingCenter, perc.Value);
            yield return new WaitForFixedUpdate ();
        }
    }

    IEnumerator StartControlFade (float _time, BoolWrapper _condition = default (BoolWrapper))
    {
        controlFading = true;
        FloatWrapper timer = new FloatWrapper(0);
        FloatWrapper perc = new FloatWrapper(0);
        Vector3 curDir = inputWorldDirection;
        while (timer.Value < _time && !_condition.Value && !wallRunning)
        {
            if (inputWorldDirection != curDir)
            {
                RunTimer(_time, timer, perc);
                Vector3 curMove = inputWorldDirection;
                move = Vector3.Lerp(curDir, curMove, perc.Value);
            }
            yield return new WaitForFixedUpdate ();
        }
        //dont disable control fading till we are grounded
        while (!_condition.Value && !wallRunning)
        {
            move = inputWorldDirection;
            yield return new WaitForFixedUpdate ();
        }
        controlFading = false;
    }

    IEnumerator StartSpeedFade (float _startSpeed, float _endSpeed, float _time, BoolWrapper _condition)
    {
        float timer = 0;
        float perc = 0;
        while (perc < 1 && _condition.Value)
        {
            timer += Time.deltaTime;
            if (timer > _time)
                timer = _time;
            perc = timer / _time;
            curSpeed = Mathf.Lerp (_startSpeed, _endSpeed, perc);
            yield return new WaitForEndOfFrame ();
        }
    }
}