﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private GameObject pauseMenu;
    [SerializeField]
    private bool freezeGameOnPause = true;

    private bool paused;
    public bool IsPaused() { return paused; }

	// Use this for initialization
	void Start ()
    {
        PauseGame(false);
	}

    private void Update()
    {
        GetInputs();
    }

    void GetInputs()
    {
        if (Input.GetButtonDown("Cancel"))
            PauseGame(!paused);
    }

    void PauseGame(bool _pause)
    {
        paused = _pause;
        SetCursorActive(_pause);
        ActivatePauseMenu(_pause);
        if (freezeGameOnPause)
        {
            if (_pause)
                Time.timeScale = 0;
            else
                Time.timeScale = 1;
        }
    }

    void ActivatePauseMenu(bool _active)
    {
        pauseMenu.SetActive(_active);
    }

    void SetCursorActive(bool _active)
    {
        Cursor.visible = _active;
        if (_active)
            Cursor.lockState = CursorLockMode.None;
        else
            Cursor.lockState = CursorLockMode.Locked;
    }
}
