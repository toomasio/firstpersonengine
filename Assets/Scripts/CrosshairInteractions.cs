﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class LayerElement
{
    public int crosshairElement = 0;

    //color
    public bool setColor;
    public Color changeColor = Color.white;
    public Color defColor = Color.white;

    //size
    public bool setSize;
    public float percentOfMax = 0;

    //image swap
    public bool swapImage;
    public Image imageToSwap;

    public bool active;

    //gui vars
    public bool opened = true;
}

[System.Serializable]
public class Interaction
{
    public int layer;
    public float minDistance = Mathf.Infinity;
    public List<LayerElement> crosshairElements = new List<LayerElement> ();
    //gui vars
    public bool opened = true;
}

public class CrosshairInteractions : MonoBehaviour
{

    [SerializeField]
    public Crosshair crosshair;
    [SerializeField]
    public List<Interaction> interactions = new List<Interaction> ();
    [SerializeField]
    private float fadeTime = 0.1f;

    private LayerMask mask;
    private Collider lastCol;

    // Use this for initialization
    void Start ()
    {
        AddToMask ();
    }

    // Update is called once per frame
    void Update ()
    {
        DetectLayersInCrossHair ();
    }

    void AddToMask ()
    {
        foreach (var item in interactions)
        {
            mask |= (1 << item.layer);
        }
    }

    void DetectLayersInCrossHair ()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay (new Vector2 (Screen.width / 2, Screen.height / 2));
        if (Physics.Raycast (ray, out hit, Mathf.Infinity, mask))
        {
            if (lastCol != hit.collider)
            {
                //reset active elements
                foreach (var interaction in interactions)
                {
                    foreach (var element in interaction.crosshairElements)
                    {
                        element.active = false;
                    }
                }
                for (int i = 0; i < interactions.Count; i++)
                {
                    if (interactions[i].layer == hit.collider.gameObject.layer)
                    {
                        if (hit.distance < interactions[i].minDistance)
                        {
                            foreach (var element in interactions[i].crosshairElements)
                            {
                                element.active = true;
                                crosshair.AddLayerElement(element);
                            }
                            crosshair.SmoothSetElementProperties(false, fadeTime);
                        }
                        
                    }

                }
            }
            lastCol = hit.collider;
        }
        else if (lastCol)
        {
            crosshair.SmoothSetElementProperties (true, fadeTime);
            lastCol = null;
        }
    }

}