﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BoolWrapper
{
    public bool Value { get; set; }
    public BoolWrapper(bool value) { this.Value = value; }
}

[System.Serializable]
public class Vector3Wrapper
{
    public Vector3 Value { get; set; }
    public Vector3Wrapper(Vector3 value) { this.Value = value; }
}

[System.Serializable]
public class FloatWrapper
{
    public float Value { get; set; }
    public FloatWrapper(float value) { this.Value = value; }
}

public class ColorContainer
{
    public List<Color> colorList = new List<Color>();
}

