﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[CustomEditor(typeof(InputManager))]

public class InputManagerEditor : Editor
{

    private InputManager source;
    private SerializedObject sourceRef;

    private SerializedProperty runInputButton;

    private int inputRun;

    private void OnEnable()
    {
        source = (InputManager)target;
        sourceRef = serializedObject;

        runInputButton = sourceRef.FindProperty("runInputButton");
    }
        

    public override void OnInspectorGUI()
    {

        GetInputNames();

    sourceRef.ApplyModifiedProperties();
        Undo.RecordObject(source, "Modified " + source + " properties.");
    }

    void GetInputNames()
    {
        var inputManager = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0];

        SerializedObject obj = new SerializedObject(inputManager);

        SerializedProperty axisArray = obj.FindProperty("m_Axes");

        string[] axisNames = new string[axisArray.arraySize];

        for (int i = 0; i < axisNames.Length; i++)
        {
            axisNames[i] = axisArray.GetArrayElementAtIndex(i).FindPropertyRelative("m_Name").stringValue;
        }

        inputRun = EditorGUILayout.Popup("Run Input Button", inputRun, axisNames);
        runInputButton.stringValue = axisArray.GetArrayElementAtIndex(inputRun).FindPropertyRelative("m_Name").stringValue;
    }
}
