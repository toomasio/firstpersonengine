﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using System.Linq;


[CustomPropertyDrawer(typeof(DataProperty))]
[CanEditMultipleObjects]
public class DataPropertyDrawer: PropertyDrawer
{
    private DataProperty source;
    private DataToSave savedData;

    private SerializedProperty id;

    //need to set field amount manually if you add more fields
    private int fieldAmount = 2;
    private float fieldSize = 16;
    private float padding = 2;


    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        //set the height of the drawer by the field size and padding
        return (fieldSize * fieldAmount)+(padding * fieldAmount);
    }

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        GetSources(property);
        SetData(property);
        //get saved Data so we can set variables
        if (!savedData && source.data || savedData != source.data)
        {
            savedData = source.data as DataToSave;
            return;
        }

        //divide all field heights by the field amount..then minus the padding
        position.height /= fieldAmount; position.height -= padding;

        //first field of the drawer
        source.data = EditorGUI.ObjectField(position,"Data To Save", source.data, typeof(DataToSave), false);

        position.y += fieldSize + padding; //offset position.y by field size
        savedData.id = EditorGUI.IntField(position,"ID", savedData.id);


        EditorGUI.EndProperty();
    }

    void DisplayFields(Rect position)
    {

    }

    void GetSources(SerializedProperty property)
    {
        if (source == null)
            source = EditorDataUtils.GetActualObjectForSerializedProperty<DataProperty>(fieldInfo, property);
    }

    void SetData(SerializedProperty property)
    {
        if (!source.data)
        {
            //Get Length of current array
            var obj = fieldInfo.GetValue(property.serializedObject.targetObject);

            object[] array = null;
            int id = property.serializedObject.targetObject.GetInstanceID();
            if (obj.GetType().IsArray)
            {
                array = (object[])obj;
                id += array.Length;
            }
                 
            Object dataRef = new Object();
            EditorDataUtils.CreateAsset<DataToSave>(id, out dataRef);
            source.data = dataRef;
            Debug.Log("Creating data asset");
            return;
        }
    }

}
