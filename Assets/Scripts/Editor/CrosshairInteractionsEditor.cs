﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[CustomEditor (typeof (CrosshairInteractions))]
public class PlayerCameraOffsetEditor : Editor
{

    private CrosshairInteractions source;
    private SerializedObject sourceRef;

    private SerializedProperty crosshair;

    private string[] crosshairElementNames;

    private void OnEnable ()
    {
        source = (CrosshairInteractions)target;
        sourceRef = serializedObject;
        GetNames ();
        GetProperties ();
    }

    public override void OnInspectorGUI ()
    {

        SetProperties ();

        sourceRef.ApplyModifiedProperties ();
        Undo.RecordObject (source, "Modified " + source + " properties.");
    }

    void GetNames ()
    {
        if (source.crosshair)
        {
            if (source.crosshair.crossHairElements.Length > 0)
            {
                crosshairElementNames = new string[source.crosshair.crossHairElements.Length];
                for (int i = 0; i < source.crosshair.crossHairElements.Length; i++)
                {
                    crosshairElementNames[i] = source.crosshair.crossHairElements[i].elementName;
                }
            }
        }

    }

    void GetProperties ()
    {
        crosshair = sourceRef.FindProperty ("crosshair");
    }

    void SetProperties ()
    {
        EditorGUILayout.Space ();
        EditorGUILayout.PropertyField (crosshair);
        if (!crosshair.objectReferenceValue)
            return;
        SetInteractions ();

    }

    void SetInteractions ()
    {
        EditorGUILayout.Space ();
        EditorGUILayout.BeginHorizontal ();
        EditorGUILayout.LabelField ("Crosshair Interaction");
        if (GUILayout.Button ("Add Interaction"))
            AddInteraction ();
        EditorGUILayout.EndHorizontal ();

        if (source.interactions.Count > 0)
        {
            for (int i = 0; i < source.interactions.Count; i++)
            {
                source.interactions[i].opened = EditorGUILayout.Foldout (source.interactions[i].opened, LayerMask.LayerToName (source.interactions[i].layer));
                if (source.interactions[i].opened)
                {
                     EditorGUI.indentLevel++;
                    //layer and element
                    source.interactions[i].layer = EditorGUILayout.LayerField ("Layer", source.interactions[i].layer);
                    EditorGUILayout.BeginHorizontal ();
                    EditorGUILayout.LabelField ("Crosshair Elements");
                    if (GUILayout.Button ("Add Element"))
                        AddElement (i);
                    EditorGUILayout.EndHorizontal ();

                    for (int ind = 0; ind < source.interactions[i].crosshairElements.Count; ind++)
                    {
                        EditorGUI.indentLevel++;
                        source.interactions[i].crosshairElements[ind].crosshairElement = EditorGUILayout.Popup ("Crosshair Element", source.interactions[i].crosshairElements[ind].crosshairElement, crosshairElementNames);

                        //color
                        source.interactions[i].crosshairElements[ind].setColor = EditorGUILayout.Toggle ("Set Color?", source.interactions[i].crosshairElements[ind].setColor);
                        if (source.interactions[i].crosshairElements[ind].setColor)
                        {
                            source.interactions[i].crosshairElements[ind].changeColor = EditorGUILayout.ColorField ("Change Color", source.interactions[i].crosshairElements[ind].changeColor);
                            source.interactions[i].crosshairElements[ind].defColor = EditorGUILayout.ColorField ("Default Color", source.interactions[i].crosshairElements[ind].defColor);
                        }
                        else
                            source.interactions[i].crosshairElements[ind].changeColor = source.interactions[i].crosshairElements[ind].defColor;

                        //size
                        source.interactions[i].crosshairElements[ind].setSize = EditorGUILayout.Toggle ("Set Size?", source.interactions[i].crosshairElements[ind].setSize);
                        if (source.interactions[i].crosshairElements[ind].setSize)
                        {
                            source.interactions[i].crosshairElements[ind].percentOfMax = EditorGUILayout.Slider ("Percent Max Width", source.interactions[i].crosshairElements[ind].percentOfMax, 0, 1);
                        }
                        else
                            source.interactions[i].crosshairElements[ind].percentOfMax = 0;

                        //remove option
                        EditorGUILayout.BeginHorizontal ();
                        EditorGUILayout.LabelField ("Remove Element?");
                        if (GUILayout.Button ("Remove Element"))
                            RemoveElement (i, source.interactions[i].crosshairElements[ind]);
                        EditorGUILayout.EndHorizontal ();
                        EditorGUI.indentLevel--;
                    }
                    //remove option
                    EditorGUILayout.BeginHorizontal ();
                    EditorGUILayout.LabelField ("Remove Interaction?");
                    if (GUILayout.Button ("Remove Interaction"))
                        RemoveColorLayer (source.interactions[i]);
                    EditorGUILayout.EndHorizontal ();
                }
            }
        }
    }

    void AddElement (int _ind)
    {
        source.interactions[_ind].crosshairElements.Add (new LayerElement ());
    }
    void RemoveElement (int _ind, LayerElement _element)
    {
        source.interactions[_ind].crosshairElements.Remove (_element);
    }
    void AddInteraction ()
    {
        source.interactions.Add (new Interaction ());
    }

    void RemoveColorLayer (Interaction _layer)
    {
        source.interactions.Remove (_layer);
    }

}