﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using System.Reflection;
using Object = UnityEngine.Object;

[CustomEditor(typeof(InspectorMethods))]

public class InspectorMethodsEditor : Editor
{

    private InspectorMethods source;
    private SerializedObject sourceRef;

    private SerializedProperty script;


    private void OnEnable()
    {
        source = (InspectorMethods)target;
        sourceRef = serializedObject;

        GetProperties();

        if (script.objectReferenceValue)
        {
            source.GetMethods();
            source.GetMethodNames();
            source.GetMethodParamsNames();
            source.DeserializeParams();
        }
            
    }

    public override void OnInspectorGUI()
    {
        sourceRef.Update();

        SetProperties();

        sourceRef.ApplyModifiedProperties();
    }

    void GetProperties()
    {
        script = sourceRef.FindProperty("script");
    }

    void SetProperties()
    {

        EditorGUILayout.PropertyField(script);
           

        if (source.script)
        {
            if (source.lastScript != source.script)
            {
                source.GetMethods();
                source.GetMethodNames();
                ResetParameterValues();
                return;
            }
            source.selectedMethod = EditorGUILayout.Popup("Chosen Method",source.selectedMethod, source.methodNames.ToArray());
            ShowParameterFields();
  
        }
    }

    
    void ShowParameterFields()
    {
        if (source.methods.Count < 1)
        {
            return;
        }
            
        if (source.lastMethod != source.selectedMethod)
        {
            ResetParameterValues();
            return;
        }

        if (source.inspectorMethodParams.Length > 0)
        {
            for (int i = 0; i < source.methodParams.Length; i++)
            {
                
                DisplayFieldByType(i, source.methodParams[i].ParameterType, source.methodParamsNames[i]);
                source.SerializeParams();
            }
        }
        else if (source.serializedObjectParams != "")
        {
            source.DeserializeParams();
            source.GetMethodParamsNames();
        }
        else
        {     
            ResetParameterValues();
        }
        
    }

    void ResetParameterValues()
    {
        source.UpdateCurMethod();
        source.InitializeParameterArray();
        source.GetMethodParamsNames();
    }

    void DisplayFieldByType(int _ind,System.Type _type, string _label)
    {
        //Vector3
        if (_type == typeof(Vector3))
        {
            var param = (Vector3)source.inspectorMethodParams[_ind];
            param = EditorGUILayout.Vector3Field(_label, param);
            source.inspectorMethodParams[_ind] = param;
        }
        //Vector2
        else if (_type == typeof(Vector2))
        {
            var param = (Vector2)source.inspectorMethodParams[_ind];
            param = EditorGUILayout.Vector2Field(_label, param);
            source.inspectorMethodParams[_ind] = param;
        }
        //float
        else if (_type == typeof(float))
        {
            var param = (float)source.inspectorMethodParams[_ind];
            param = EditorGUILayout.FloatField(_label, param);
            source.inspectorMethodParams[_ind] = param;
        }
        //int
        else if (_type == typeof(int))
        {
            var param = (int)source.inspectorMethodParams[_ind];
            param = EditorGUILayout.IntField(_label, param);
            source.inspectorMethodParams[_ind] = param;
        }
        //string
        else if (_type == typeof(string))
        {
            var param = (string)source.inspectorMethodParams[_ind];
            param = EditorGUILayout.TextField(_label, param);
            source.inspectorMethodParams[_ind] = param;
        }
        //Quaternion
        else if (_type == typeof(Quaternion))
        {
            var value = (Quaternion)source.inspectorMethodParams[_ind];
            Vector4 v4Value = new Vector4(value.x,value.y,value.z,value.w);
            v4Value = EditorGUILayout.Vector4Field(_label, v4Value);
            Quaternion qValue = new Quaternion(v4Value.x, v4Value.y, v4Value.z, v4Value.w);
            source.inspectorMethodParams[_ind] = qValue;
        }
        //vector4
        else if (_type == typeof(Vector4))
        {
            var param = (Vector4)source.inspectorMethodParams[_ind];
            param = EditorGUILayout.Vector4Field(_label, param);
            source.inspectorMethodParams[_ind] = param;
        }
        //bool
        else if (_type == typeof(bool))
        {
            var param = (bool)source.inspectorMethodParams[_ind];
            param = EditorGUILayout.Toggle(_label, param);
            source.inspectorMethodParams[_ind] = param;
        }
        //Rect
        else if (_type == typeof(Rect))
        {
            var param = (Rect)source.inspectorMethodParams[_ind];
            param = EditorGUILayout.RectField(_label, param);
            source.inspectorMethodParams[_ind] = param;
        }
        //Transform
        else if (_type == typeof(Transform))
        {
            var param = (Transform)source.inspectorMethodParams[_ind];
            param = (Transform)EditorGUILayout.ObjectField(_label, param, typeof(Transform), true);
            source.inspectorMethodParams[_ind] = param;
        }
        //GameObject
        else if (_type == typeof(GameObject))
        {
            var param = (GameObject)source.inspectorMethodParams[_ind];
            param = (GameObject)EditorGUILayout.ObjectField(_label, param, typeof(GameObject), true);
            source.inspectorMethodParams[_ind] = param;
        }

    }

}
