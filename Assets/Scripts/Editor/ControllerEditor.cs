﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[CustomEditor(typeof(Controller))]

public class ControllerEditor : Editor
{

    private Controller source;
    private SerializedObject sourceRef;

    //movement
    private SerializedProperty inputAxisLeftRight;
    private SerializedProperty inputAxisForBack;
    private SerializedProperty speed;
    private SerializedProperty enableRun;
    private SerializedProperty inputButtonRun;
    private SerializedProperty onlyRunForward;
    private SerializedProperty runSpeed;
    //wall running
    private SerializedProperty enableWallRunning;
    private SerializedProperty zRotAmount;
    private SerializedProperty transitionTime;
    private SerializedProperty upwardPushAssist;

    //rotation
    private SerializedProperty inputAxisLookLeftRight;
    private SerializedProperty inputAxisLookUpDown;
    private SerializedProperty rotateSensitivity;
    private SerializedProperty controllerCamera;
    private SerializedProperty camRotateLimit;
    private SerializedProperty invertLook;
    //crouch
    private SerializedProperty enableCrouch;
    private SerializedProperty inputButtonCrouch;
    private SerializedProperty crouchSpeed;
    private SerializedProperty crouchSizeTime;
    private SerializedProperty crouchSpeedTime;
    private SerializedProperty crouchSize;
    //jumping
    private SerializedProperty inputButtonJump;
    private SerializedProperty jumpPower;
    private SerializedProperty inAirControlFadeInTime;
    private SerializedProperty gravityMultiplier;
    //ground
    private SerializedProperty groundMask;
    private SerializedProperty groundBoxSize;
    private SerializedProperty groundBoxCenter;
    //ceiling
    private SerializedProperty ceilingMask;
    private SerializedProperty ceilingBoxSize;
    private SerializedProperty ceilingBoxCenter;
    //wall
    private SerializedProperty stopMovementOnWallHit;
    private SerializedProperty wallMask;
    private SerializedProperty stopPercent;
    //wall RUnning detect
    private SerializedProperty wallRunGroundBoxSize;
    private SerializedProperty wallRunGroundBoxCenter;

    //foldouts
    private bool movementFoldout;
    private bool rotationFoldout;
    private bool crouchFoldout;
    private bool jumpingFoldout;
    private bool wallRunFoldout;
    private bool wallRunDetectFoldout;
    private bool groundFoldout;
    private bool ceilingFoldout;
    private bool wallFoldout;

    //temp axis stuff
    private SerializedProperty axisArray;
    private string[] inputAxisNames;
    private SerializedProperty inputLeftRight;
    private SerializedProperty inputForBack;
    private SerializedProperty inputLookLeftRight;
    private SerializedProperty inputLookUpDown;
    private SerializedProperty inputRun;
    private SerializedProperty inputCrouch;
    private SerializedProperty inputJump;

    //temp vars
    private CapsuleCollider col;

    private void OnEnable()
    {
        source = (Controller)target;
        sourceRef = serializedObject;
        GetComponents();
        GetInputNames();
        GetProperties();
    }

    public override void OnInspectorGUI()
    { 
        SetProperties();
        sourceRef.ApplyModifiedProperties();
    }

    void GetInputNames()
    {

        var inputManager = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0];
        SerializedObject obj = new SerializedObject(inputManager);
        axisArray = obj.FindProperty("m_Axes");
        inputAxisNames = new string[axisArray.arraySize];

        for (int i = 0; i < inputAxisNames.Length; i++)
        {
            inputAxisNames[i] = axisArray.GetArrayElementAtIndex(i).FindPropertyRelative("m_Name").stringValue;
        }

    }

    void GetComponents()
    {
        col = source.GetComponent<CapsuleCollider>();
    }

    void GetProperties()
    {
        //movement
        inputAxisLeftRight = sourceRef.FindProperty("inputAxisLeftRight");
        inputAxisForBack = sourceRef.FindProperty("inputAxisForBack");
        speed = sourceRef.FindProperty("speed");
        enableRun = sourceRef.FindProperty("enableRun");
        inputButtonRun = sourceRef.FindProperty("inputButtonRun");
        onlyRunForward = sourceRef.FindProperty("onlyRunForward");
        runSpeed = sourceRef.FindProperty("runSpeed");
        //wall running
        enableWallRunning = sourceRef.FindProperty("enableWallRunning");
        zRotAmount = sourceRef.FindProperty("zRotAmount");
        transitionTime = sourceRef.FindProperty("transitionTime");
        upwardPushAssist = sourceRef.FindProperty("upwardPushAssist");

        //rotation
        inputAxisLookLeftRight = sourceRef.FindProperty("inputAxisLookLeftRight");
        inputAxisLookUpDown = sourceRef.FindProperty("inputAxisLookUpDown");
        rotateSensitivity = sourceRef.FindProperty("rotateSensitivity");
        controllerCamera = sourceRef.FindProperty("controllerCamera");
        camRotateLimit = sourceRef.FindProperty("camRotateLimit");
        invertLook = sourceRef.FindProperty("invertLook");
        //crouch
        enableCrouch = sourceRef.FindProperty("enableCrouch");
        inputButtonCrouch = sourceRef.FindProperty("inputButtonCrouch");
        crouchSpeed = sourceRef.FindProperty("crouchSpeed");
        crouchSizeTime = sourceRef.FindProperty("crouchSizeTime");
        crouchSpeedTime = sourceRef.FindProperty("crouchSpeedTime");
        crouchSize = sourceRef.FindProperty("crouchSize");
        //jumping
        inputButtonJump = sourceRef.FindProperty("inputButtonJump");
        jumpPower = sourceRef.FindProperty("jumpPower");
        inAirControlFadeInTime = sourceRef.FindProperty("inAirControlFadeInTime");
        gravityMultiplier = sourceRef.FindProperty("gravityMultiplier");
        //ground
        groundMask = sourceRef.FindProperty("groundMask");
        groundBoxSize = sourceRef.FindProperty("groundBoxSize");
        groundBoxCenter = sourceRef.FindProperty("groundBoxCenter");

        //ceiling
        ceilingMask = sourceRef.FindProperty("ceilingMask");
        ceilingBoxSize = sourceRef.FindProperty("ceilingBoxSize");
        ceilingBoxCenter = sourceRef.FindProperty("ceilingBoxCenter");
        //wall
        stopMovementOnWallHit = sourceRef.FindProperty("stopMovementOnWallHit");
        wallMask = sourceRef.FindProperty("wallMask");
        stopPercent = sourceRef.FindProperty("stopPercent");
        //wall run detect
        wallRunGroundBoxSize = sourceRef.FindProperty("wallRunGroundBoxSize");
        wallRunGroundBoxCenter = sourceRef.FindProperty("wallRunGroundBoxCenter");

        //axis stuff
        inputLeftRight = sourceRef.FindProperty("inputLeftRight");
        inputForBack = sourceRef.FindProperty("inputForBack");
        inputLookUpDown = sourceRef.FindProperty("inputLookUpDown");
        inputLookLeftRight = sourceRef.FindProperty("inputLookLeftRight");
        inputRun = sourceRef.FindProperty("inputRun");
        inputCrouch = sourceRef.FindProperty("inputCrouch");
        inputJump = sourceRef.FindProperty("inputJump");

    }

    void SetProperties()
    {
        EditorGUILayout.LabelField("-----------------------");
        EditorGUILayout.LabelField("Locomotion");
        EditorGUILayout.LabelField("-----------------------");
        EditorGUI.indentLevel++;
        //movement
        movementFoldout = EditorGUILayout.Foldout(movementFoldout, "Movement");
        if (movementFoldout)
        {
            EditorGUI.indentLevel++;
            //hor axis enum
            inputLeftRight.intValue = EditorGUILayout.Popup("Input Axis Left/Right", inputLeftRight.intValue, inputAxisNames);
            SerializedProperty elementHor = axisArray.GetArrayElementAtIndex(inputLeftRight.intValue);
            inputAxisLeftRight.stringValue = elementHor.FindPropertyRelative("m_Name").stringValue;

            //ver axis enum
            inputForBack.intValue = EditorGUILayout.Popup("Input Axis Forward/Backward", inputForBack.intValue, inputAxisNames);
            SerializedProperty elementVer = axisArray.GetArrayElementAtIndex(inputForBack.intValue);
            inputAxisForBack.stringValue = elementVer.FindPropertyRelative("m_Name").stringValue;

            EditorGUILayout.PropertyField(speed);
            EditorGUILayout.PropertyField(enableRun);
            if (!enableRun.boolValue)
                enableWallRunning.boolValue = false;
            if (enableRun.boolValue)
            {
                //run enum
                inputRun.intValue = EditorGUILayout.Popup("Input Button Run", inputRun.intValue, inputAxisNames);
                SerializedProperty elementRun = axisArray.GetArrayElementAtIndex(inputRun.intValue);
                inputButtonRun.stringValue = elementRun.FindPropertyRelative("m_Name").stringValue;

                EditorGUILayout.PropertyField(onlyRunForward);
                EditorGUILayout.PropertyField(runSpeed);
                    
            }
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.Space();
        //rotation
        rotationFoldout = EditorGUILayout.Foldout(rotationFoldout, "Rotation");
        if (rotationFoldout)
        {
            EditorGUI.indentLevel++;
            //look hor axis enum
            inputLookLeftRight.intValue = EditorGUILayout.Popup("Input Look Left/Right", inputLookLeftRight.intValue, inputAxisNames);
            SerializedProperty elementLR = axisArray.GetArrayElementAtIndex(inputLookLeftRight.intValue);
            inputAxisLookLeftRight.stringValue = elementLR.FindPropertyRelative("m_Name").stringValue;

            //look ver axis enum
            inputLookUpDown.intValue = EditorGUILayout.Popup("Input Look Up/Down", inputLookUpDown.intValue, inputAxisNames);
            SerializedProperty elementUD = axisArray.GetArrayElementAtIndex(inputLookUpDown.intValue);
            inputAxisLookUpDown.stringValue = elementUD.FindPropertyRelative("m_Name").stringValue;

            EditorGUILayout.PropertyField(rotateSensitivity);
            EditorGUILayout.PropertyField(controllerCamera);
                EditorGUILayout.PropertyField(camRotateLimit);
                EditorGUILayout.PropertyField(invertLook);
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.Space();
        //jumping
        jumpingFoldout = EditorGUILayout.Foldout(jumpingFoldout, "Jumping");
        if (jumpingFoldout)
        {
            EditorGUI.indentLevel++;
            //jump enum
            inputJump.intValue = EditorGUILayout.Popup("Input Button Jump", inputJump.intValue, inputAxisNames);
            SerializedProperty elementJump = axisArray.GetArrayElementAtIndex(inputJump.intValue);
            inputButtonJump.stringValue = elementJump.FindPropertyRelative("m_Name").stringValue;

            EditorGUILayout.PropertyField(jumpPower);
            EditorGUILayout.PropertyField(inAirControlFadeInTime);
            EditorGUILayout.PropertyField(gravityMultiplier);
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.Space();
        //crouch
        if (!enableCrouch.boolValue)
            EditorGUILayout.PropertyField(enableCrouch);
        else
        {
            crouchFoldout = EditorGUILayout.Foldout(crouchFoldout, "Crouch");
            if (crouchFoldout)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(enableCrouch);

                //crouch enum
                inputCrouch.intValue = EditorGUILayout.Popup("Input Button Crouch", inputCrouch.intValue, inputAxisNames);
                SerializedProperty elementCrouch = axisArray.GetArrayElementAtIndex(inputCrouch.intValue);
                inputButtonCrouch.stringValue = elementCrouch.FindPropertyRelative("m_Name").stringValue;

                EditorGUILayout.PropertyField(crouchSpeed);
                EditorGUILayout.PropertyField(crouchSizeTime);
                EditorGUILayout.PropertyField(crouchSpeedTime);
                crouchSize.floatValue = EditorGUILayout.Slider("CrouchSizePercent", crouchSize.floatValue, 0.5f, 1);
                EditorGUI.indentLevel--;
            }
        }
        EditorGUILayout.Space();
        //wall RUnning
        if (enableRun.boolValue)
        {
            if (!enableWallRunning.boolValue)
                EditorGUILayout.PropertyField(enableWallRunning);
            else
            {
                wallRunFoldout = EditorGUILayout.Foldout(wallRunFoldout, "Wall Running");
                if (wallRunFoldout)
                {
                    EditorGUI.indentLevel++;
                    EditorGUILayout.PropertyField(enableWallRunning);
                        EditorGUILayout.PropertyField(zRotAmount);
                        EditorGUILayout.PropertyField(transitionTime);
                        EditorGUILayout.PropertyField(upwardPushAssist);
                    EditorGUI.indentLevel--;
                }
            }
            
        }
        
        EditorGUI.indentLevel--;
        EditorGUILayout.LabelField("-----------------------");
        EditorGUILayout.LabelField("Physics Detection");
        EditorGUILayout.LabelField("-----------------------");
        EditorGUI.indentLevel++;
        //ground
        groundFoldout = EditorGUILayout.Foldout(groundFoldout, "Ground Detection");
        if (groundFoldout)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(groundMask);
            EditorGUILayout.PropertyField(groundBoxSize);
            EditorGUILayout.PropertyField(groundBoxCenter);
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.Space();
        //ceiling
        ceilingFoldout = EditorGUILayout.Foldout(ceilingFoldout, "Ceiling Detection");
        if (ceilingFoldout)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(ceilingMask);
            EditorGUILayout.PropertyField(ceilingBoxSize);
            EditorGUILayout.PropertyField(ceilingBoxCenter);
            EditorGUI.indentLevel--;

        }
        EditorGUILayout.Space();
        //walls

            wallFoldout = EditorGUILayout.Foldout(wallFoldout, "Side Detection");
            if (wallFoldout)
            {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(wallMask);
                EditorGUILayout.PropertyField(stopMovementOnWallHit);
                if (stopMovementOnWallHit.boolValue)
                    stopPercent.floatValue = EditorGUILayout.Slider("Stop Percent",stopPercent.floatValue,0,1);
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.Space();
        if (enableWallRunning.boolValue && enableRun.boolValue)
        {
            //wall run detect
            wallRunDetectFoldout = EditorGUILayout.Foldout(wallRunDetectFoldout, "Wall Run Detection");
            if (wallRunDetectFoldout)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(wallRunGroundBoxCenter);
                EditorGUILayout.PropertyField(wallRunGroundBoxSize);
                EditorGUI.indentLevel--;
            }
        }

    }

    private void OnSceneGUI()
    {
        //draw wallchecks
        Handles.color = Color.yellow;
        if (!Application.isPlaying)
        {
            Vector3 extentsY = new Vector3(0,col.bounds.extents.y,0);
            source.curWallCheckPoint1 = source.transform.TransformPoint(extentsY - (col.center / 4));
            source.curWallCheckPoint2 = source.transform.TransformPoint(extentsY + (col.center / 4));
            source.curWallCheckRadius = col.radius * 0.7f;

        }
        DrawWireSphere(source.curWallCheckPoint1, source.curWallCheckRadius);
        DrawWireSphere(source.curWallCheckPoint2, source.curWallCheckRadius);

        //draw grounded
        if (!Application.isPlaying)
            source.curGroundCenter = groundBoxCenter.vector3Value;
        Handles.matrix = Matrix4x4.TRS(source.transform.TransformPoint(source.curGroundCenter), source.transform.rotation, Vector3.one);
        Handles.color = Color.cyan;
        Handles.DrawWireCube(Vector3.zero, groundBoxSize.vector3Value);
        //draw wall running ground box
        if (enableWallRunning.boolValue)
        {
            Handles.matrix = Matrix4x4.TRS(source.transform.TransformPoint(wallRunGroundBoxCenter.vector3Value), source.transform.rotation, Vector3.one);
            Handles.color = Color.red;
            Handles.DrawWireCube(Vector3.zero, wallRunGroundBoxSize.vector3Value);
        }
        //draw ceiling
        if (!Application.isPlaying)
            source.curCeilingCenter = ceilingBoxCenter.vector3Value;
        Handles.matrix = Matrix4x4.TRS(source.transform.TransformPoint(source.curCeilingCenter), source.transform.rotation, Vector3.one);
        Handles.color = Color.blue;
        Handles.DrawWireCube(Vector3.zero, ceilingBoxSize.vector3Value);

    }

    void DrawWireSphere(Vector3 _center, float _radius)
    {
        Handles.DrawWireDisc(_center, source.transform.up, _radius);
        Handles.DrawWireDisc(_center, source.transform.forward, _radius);
        Handles.DrawWireDisc(_center, source.transform.right, _radius);
    }

}
