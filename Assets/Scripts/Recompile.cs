﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
 
public class Recompile : MonoBehaviour 
{
   
    [MenuItem("CustomTools/Compile &c")]
    static void Compile () 
	{
 
        string path = Directory.GetCurrentDirectory() + "/Library/ScriptAssemblies";
        DirectoryInfo dir = new DirectoryInfo(path);
        FileInfo[] info = dir.GetFiles("*.*");
        foreach (FileInfo f in info)
        {
            Debug.Log(f + " deleted");
            f.Delete();
        }
    }
}