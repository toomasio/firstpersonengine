﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField]
    private MenuManager menuManager;
    public MenuManager GetMenuManager() { return menuManager; }

    private void Awake()
    {
        instance = this;
    }

}
