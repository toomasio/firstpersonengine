﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class CrossHairElement
{
    public string elementName;
    public RectTransform parentRect;
    public Vector2 maxSize;
    public Image[] images;

    public Vector2 startSize;
}

public class Crosshair : MonoBehaviour
{
    public CrossHairElement[] crossHairElements;
    private List<LayerElement> affectedLayerElements = new List<LayerElement> ();

    private void Start ()
    {
        GetStartSizes ();
    }

    void GetStartSizes ()
    {
        foreach (var item in crossHairElements)
        {
            item.startSize = item.parentRect.sizeDelta;
        }
    }

    void RunTimer (float _time, FloatWrapper _timer, FloatWrapper _perc)
    {
        _timer.Value += Time.deltaTime;
        if (_timer.Value > _time)
            _timer.Value = _time;
        _perc.Value = _timer.Value / _time;
    }

    public void SmoothSetElementProperties (bool _reset, float _time = default (float))
    {
        StopAllCoroutines();
        if (_reset)
        {
            foreach (var element in affectedLayerElements)
            {
                element.active = false;
            }
        }
        StartCoroutine (StartSmoothSetElementProperties (_time, _reset));
    }

    IEnumerator StartSmoothSetElementProperties (float _time, bool _reset)
    {
        List<ColorContainer> startColorContainer = new List<ColorContainer> ();

        List<Vector2> startSizes = new List<Vector2> ();
        List<Vector2> curSizes = new List<Vector2> ();
        List<Vector2> endSizes = new List<Vector2> ();
        List<CrossHairElement> elements = new List<CrossHairElement> ();
        List<Color> endColors = new List<Color> ();
        List<Color> defColors = new List<Color> ();
        List<bool> actives = new List<bool> ();
        foreach (var layerElement in affectedLayerElements)
        {
            //get elements
            CrossHairElement element = crossHairElements[layerElement.crosshairElement];
            elements.Add (element);
            //active
            actives.Add (layerElement.active);
            //get cur size
            Vector2 curSize = element.parentRect.sizeDelta;
            curSizes.Add (curSize);
            //get start size
            Vector2 startSize = element.startSize;
            startSizes.Add (startSize);
            //get end size
            Vector2 add = (element.maxSize - startSize) * layerElement.percentOfMax;
            Vector2 endSize = startSize + add;
            endSizes.Add (endSize);
            //end color
            endColors.Add (layerElement.changeColor);
            //def color
            defColors.Add (layerElement.defColor);
            //setupColorContainer
            startColorContainer.Add (new ColorContainer ());
        }
        //get Start and end Colors
        foreach (var element in elements)
        {
            foreach (var con in startColorContainer)
            {
                foreach (var image in element.images)
                {
                    con.colorList.Add (image.color);
                }
            }

        }
        FloatWrapper timer = new FloatWrapper (0);
        FloatWrapper perc = new FloatWrapper (0);
        while (timer.Value < _time)
        {
            RunTimer (_time, timer, perc);
            for (int i = 0; i < elements.Count; i++)
            {
                if (actives[i])
                    elements[i].parentRect.sizeDelta = Vector2.Lerp (curSizes[i], endSizes[i], perc.Value);
                else
                    elements[i].parentRect.sizeDelta = Vector2.Lerp (curSizes[i], startSizes[i], perc.Value);

                for (int ind = 0; ind < elements[i].images.Length; ind++)
                {
                    if (actives[i])
                        elements[i].images[ind].color = Color.Lerp (startColorContainer[i].colorList[ind], endColors[i], perc.Value);
                    else
                        elements[i].images[ind].color = Color.Lerp (startColorContainer[i].colorList[ind], defColors[i], perc.Value);
                }
            }

            yield return new WaitForEndOfFrame ();
        }
        if (_reset)
            affectedLayerElements.Clear ();
    }

    CrossHairElement FindElementByName (string _name)
    {
        foreach (var item in crossHairElements)
        {
            if (item.elementName == _name)
                return item;
        }
        return null;
    }

    public void AddLayerElement (LayerElement _element)
    {
        if (!affectedLayerElements.Contains (_element))
            affectedLayerElements.Add (_element);
        else
        {
            affectedLayerElements.Remove (_element);
            affectedLayerElements.Add (_element);
        }
    }
}