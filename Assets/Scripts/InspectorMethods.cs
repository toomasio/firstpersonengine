﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Object = UnityEngine.Object;
using System;
using FullSerializer;

public class InspectorMethods : MonoBehaviour 
{
    [fsProperty]
    public Object script;

    [fsProperty]
    public MethodInfo[] allMethods = new MethodInfo[0];
    [fsProperty]
    public List<MethodInfo> methods = new List<MethodInfo>();
    public List<string> methodNames = new List<string>();
    [fsProperty]
    public ParameterInfo[] methodParams = new ParameterInfo[0];
	
	public string[] methodParamsNames = new string[0];
    [fsProperty]
    public int selectedMethod;
    public int lastMethod;
    [fsProperty]
	public object[] inspectorMethodParams;
    [fsProperty]
    public object[] paramsToCall;
    public bool initialized;
    [fsProperty]
    public MethodInfo curMethod;
    [fsProperty]
    public Object lastScript;
    [SerializeField]
    public string serializedObjectParams;

    fsSerializer serializer = new fsSerializer();


    private void Start()
    {
        DeserializeParams();
        Debug.Log(inspectorMethodParams[0]);
        DoInvokeMethod();
    }

    public void GetMethods()
    {
        var type = script.GetType();
        lastScript = script;
        allMethods = type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.SetProperty);

        UpdateCurMethod();
    }

    public void UpdateCurMethod()
    {
        curMethod = allMethods[selectedMethod];
        lastMethod = selectedMethod;
        methodParams = curMethod.GetParameters();
        Debug.Log(curMethod.Name);
    }

    void DoInvokeMethod()
    {
        GetMethods();
        curMethod.Invoke(script,inspectorMethodParams);
    }

    public void GetMethodNames()
    {
        
        for (int i = 0; i < allMethods.Length; i++)
        {
            ParameterInfo[] paramsInfo = allMethods[i].GetParameters();
            string joinedNames = "";
            if (paramsInfo.Length > 0)
            {
                string[] paramNames = new string[paramsInfo.Length];
                for (int ind = 0; ind < paramsInfo.Length; ind++)
                {
                    string typeName = paramsInfo[ind].ParameterType.ToString();
                    paramNames[ind] = " " + typeName + ",";
                    joinedNames += paramNames[ind];
                }

            }
            methodNames.Add(allMethods[i].Name + joinedNames);
            methods.Add(allMethods[i]);
        }
    }

    public void DeserializeParams()
    {
        fsData data = fsJsonParser.Parse(serializedObjectParams);

        object deserialized = null;
        serializer.TryDeserialize(data, typeof(object[]), ref deserialized);

        inspectorMethodParams = (object[])deserialized;
    }

    public void SerializeParams()
    {
        fsData data;
        serializer.TrySerialize(typeof(object[]), inspectorMethodParams, out data);
        serializedObjectParams = fsJsonPrinter.CompressedJson(data);
    }

    public void InitializeParameterArray()
    {
        inspectorMethodParams = new object[methodParams.Length];
        for (int i = 0; i < methodParams.Length; i++)
        {
            var type = methodParams[i].ParameterType;
            if (type != typeof(Transform))
                inspectorMethodParams[i] = System.Activator.CreateInstance(type); 
            Debug.Log(inspectorMethodParams[i]);
        }
    }

    public void GetMethodParamsNames()
    {
        if (methodParams == null)
        {
            return;
        }  

        methodParamsNames = new string[methodParams.Length];
        for (int i = 0; i < methodParams.Length; i++)
        {
            methodParamsNames[i] = methodParams[i].Name;
        }
    }

    private string GetTypeName(string _typeName)
    {
        if (_typeName == "System.Single")
            return "float";
        else
            return null;
    }


}
