﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataProperty
{
    [SerializeField]
    public Object data;
}
